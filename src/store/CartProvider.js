import React, { useReducer } from "react";
import CartContext from "./cart-context";

const cartReducer = (state, action) => {
  if (action.type === "INPUT_ITEM") {
    const currData = state.items.find((row) => row.id === action.item.id);

    if (currData) {
      const newArray = [...state.items].map((obj) => {
        if (obj.id === action.item.id) {
          let newQty = (obj.qty += 1);
          let newAmount = obj.price * newQty;

          obj.qty = newQty;
          obj.amount = newAmount;
        }

        return obj;
      });

      return {
        items: newArray,
        totalAmount: newArray.reduce((currNumber, item) => {
          return currNumber + item.amount;
        }, 0),
      };
    } else {
      const newArray = [...state.items];
      newArray.unshift(action.item);

      return {
        items: newArray,
        totalAmount: newArray.reduce((currNumber, item) => {
          return currNumber + item.amount;
        }, 0),
      };
    }
  } else if (action.type === "REMOVE_ITEM") {
    const newArray = [...state.items].filter((obj) => {
      return obj.id !== action.id;
    });

    return {
      items: newArray,
      totalAmount: newArray.reduce((currNumber, item) => {
        return currNumber + item.amount;
      }, 0),
    };
  }
  return { items: [], totalAmount: 0 };
};

const CartProvider = (props) => {
  const [cartState, cartDispatch] = useReducer(cartReducer, {
    items: [],
    totalAmount: 0,
  });
  const addItemHandler = (item) => {
    cartDispatch({ type: "INPUT_ITEM", item: item });
  };
  const removeItemHandler = (id) => {
    cartDispatch({ type: "REMOVE_ITEM", id: id });
  };

  const cartContext = {
    items: cartState.items,
    totalAmount: cartState.totalAmount,
    addItem: addItemHandler,
    removeItem: removeItemHandler,
  };

  return (
    <CartContext.Provider value={cartContext}>
      {props.children}
    </CartContext.Provider>
  );
};

export default CartProvider;
