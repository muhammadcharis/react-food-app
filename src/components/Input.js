import React from "react";

const Input = React.forwardRef((props, ref) => {
  return (
    <div className="flex items-center gap-2 mb-[0.5rem] px-3">
      <label
        className="font-bold first-letter:uppercase"
        htmlFor={props.input.id}>
        {props.label}
      </label>
      <input
        ref={ref}
        {...props.input}
        className={[
          "input input-xs max-w-xs",
          props.isValid ? "" : "input-bordered input-error",
        ]}
      />
    </div>
  );
});

export default Input;
