import React from "react";
import AvailableMeals from "./AvailableMeals";

const Meals = () => {
  return (
    <div className="container min-h-screen mx-auto py-10 my-5 ">
      <div className="card glass shadow-xl w-full">
        <div className="card-body">
          <AvailableMeals />
        </div>
      </div>
    </div>
  );
};

export default Meals;
