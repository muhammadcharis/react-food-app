import React, { useRef, useState } from "react";
import Input from "../Input";

const FormMeats = (props) => {
  const [isValid, setIsValid] = useState(true);
  const inputQtyRef = useRef();
  const submitHandler = (e) => {
    e.preventDefault();
    const selectedAmount = inputQtyRef.current.value;
    const selectedAmountNumber = +selectedAmount;
    if (selectedAmount.trim().length === 0 && selectedAmountNumber < 1) {
      setIsValid(false);
      return false;
    }

    setIsValid(true);
    props.onAddCart({ selectedAmountNumber, inputQtyRef });
    //const data = {};
  };
  return (
    <form onSubmit={submitHandler}>
      <div>
        <Input
          label={`Qty`}
          ref={inputQtyRef}
          isValid={isValid}
          input={{
            type: "number",
            id: `${props.id}`,
            min: 1,
            max: 5,
            step: 1,
            defaultValue: 1,
          }}
        />
        <button
          type="submit"
          className="btn w-32 border-amber-800  border bg-white text-amber-700 rounded-full hover:bg-amber-800 hover:text-white hover:border-amber-700">
          Add
        </button>
      </div>
    </form>
  );
};

export default FormMeats;
