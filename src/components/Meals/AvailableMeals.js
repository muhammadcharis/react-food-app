import React, { useContext } from "react";
import FormMeats from "./FormMeats";
import CartContext from "../../store/cart-context";

const DUMMY_MEALS = [
  {
    id: "m1",
    name: "Sushi",
    description: "Finest fish and veggies",
    price: 22.99,
  },
  {
    id: "m2",
    name: "Schnitzel",
    description: "A german specialty!",
    price: 16.5,
  },
  {
    id: "m3",
    name: "Barbecue Burger",
    description: "American, raw, meaty",
    price: 12.99,
  },
  {
    id: "m4",
    name: "Green Bowl",
    description: "Healthy...and green...",
    price: 18.99,
  },
];

const AvailableMeals = (props) => {
  const cartCtx = useContext(CartContext);

  const onAddCart = (data) => {
    const qty = data.selectedAmountNumber;
    const id = data.inputQtyRef.current.id;
    const menu = DUMMY_MEALS.find((row) => {
      return row.id === id;
    });

    cartCtx.addItem({
      id: menu.id,
      name: menu.name,
      description: menu.description,
      qty,
      price: qty * menu.price,
      amount: qty * menu.price,
    });
  };
  return (
    <ul className="list-none">
      {DUMMY_MEALS.map((row, index) => {
        return (
          <li key={row.id}>
            <div className="flex flex-col w-full border-opacity-50">
              <div className="flex h-20 card rounded-box place-items-start justify-between flex-row">
                <div>
                  <h1 className="font-bold text-xl">{row.name}</h1>
                  <p className="italic">{row.description}</p>
                  <p className="font-semibold text-amber-800">${row.price}</p>
                </div>
                <div>
                  <FormMeats id={row.id} onAddCart={onAddCart} />
                </div>
              </div>

              {index !== DUMMY_MEALS.length - 1 && <div className="divider" />}
            </div>
          </li>
        );
      })}
    </ul>
  );
};

export default AvailableMeals;
