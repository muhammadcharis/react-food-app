import React, { useContext } from "react";
import Modal from "../Modal";
import CartContext from "../../store/cart-context";

const CartModal = (props) => {
  const cartCtx = useContext(CartContext);
  return (
    <Modal id={`cart-modal`} isShowModal={props.isShowModal}>
      <h3 className="font-bold text-lg">List Order</h3>
      <div className="py-4">
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th>#</th>
                <th>Item</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {cartCtx.items.map((row, index) => {
                return (
                  <tr key={row.id}>
                    <th>{index + 1}</th>
                    <td>{row.name}</td>
                    <td>{row.qty}</td>
                    <td>{row.qty * row.price}</td>
                    <td>{row.amount}</td>
                    <td>
                      <button
                        className="flex justify-center items-center w-full"
                        onClick={(e) => {
                          cartCtx.removeItem(row.id);
                        }}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6 text-red-700 ">
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                          />
                        </svg>
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
      <div className="modal-action">
        <label
          htmlFor="my-modal-6"
          className="btn bg-white text-slate-600 hover:bg-white rounded-full"
          onClick={() => {
            props.onCloseModal();
          }}>
          Cancel
        </label>
        <label htmlFor="my-modal-6" className="btn rounded-full">
          Order
        </label>
      </div>
    </Modal>
  );
};

CartModal.propTypes = {};

export default CartModal;
