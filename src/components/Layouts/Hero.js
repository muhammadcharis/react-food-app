import React from "react";
import img1 from "../../assets/images/meals.jpeg";

const Hero = (props) => {
  return (
    <div
      className="hero min-h-[80vh] md:min-h-[10vh]"
      style={{ backgroundImage: `url(${img1})` }}>
      <div className="hero-content text-center text-neutral-content md:mt-20 h-60 relative w-full">
        <div className="flex flex-col glass rounded-lg px-6 py-12 absolute top-16 ">
          <div className="w-full">
            <h1 className="mb-5 text-5xl font-bold">
              Delicious Food, Delivered To You
            </h1>
          </div>
          <div>
            <p>
              Choose your favorite meal from our broad selection of available
              meals and enjoy a delicious lunch or dinner at home.
            </p>
            <p>
              All our meals are cooked with high-quality ingredients,
              just-in-time and of course by experienced chefs!
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
