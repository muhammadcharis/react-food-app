import React, { Fragment } from "react";
import Navbar from "./Navbar";
import Hero from "./Hero";

const Header = (props) => {
  return (
    <Fragment>
      <Navbar />
      <Hero />
    </Fragment>
  );
};

export default Header;
