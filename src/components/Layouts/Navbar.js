import React from "react";
import ReactDOM from "react-dom";
import CartButton from "./CartButton";

const NavbarDom = (props) => {
  return (
    <div className="navbar bg-base-100 fixed z-10 top-0">
      <div className="flex-1">
        <p className="btn btn-ghost normal-case text-xl">Larasati Catering</p>
      </div>
      <div className="flex-none">
        <CartButton />
      </div>
    </div>
  );
};

const Navbar = (props) => {
  return ReactDOM.createPortal(
    <NavbarDom />,
    document.getElementById("navbar_root"),
  );
};

export default Navbar;
