import React from "react";
import ReactDom from "react-dom";

const Modal = (props) => {
  return ReactDom.createPortal(
    <React.Fragment>
      <input type="checkbox" id={props.id} className="modal-toggle" />
      <div
        className={[
          "modal modal-bottom sm:modal-middle",
          props.isShowModal ? "modal-open" : "",
        ].join(" ")}>
        <div className="modal-box">{props.children}</div>
      </div>
    </React.Fragment>,
    document.getElementById("modal_root"),
  );
};

export default Modal;
