import Header from "./components/Layouts/Header";
import Meals from "./components/Meals/Meals";
import React, { Fragment } from "react";
import CartProvider from "./store/CartProvider";

function App() {
  return (
    <CartProvider>
      <Header />
      <Meals />
    </CartProvider>
  );
}

export default App;
